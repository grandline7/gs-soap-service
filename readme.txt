1. example project
  - Spring 4 SOAP WebService(spring-ws 사용)

2. IDEA
  - Intellij 2016.2 EAP

3. 사용 버전
  - JDK 1.8, spring 4.2.7, Gradle 2.3

4. 주요 라이브러리
  - spring boot, spring-ws-*, wsdl4j

5. Application
  - WSDL 기반 SOAP 웹 서비스
  - 웹서비스 domain 은 Spring-WS 를 WSDL로 자동적으로 익스포트되도록 하는 XML스키마파일(XSD)에 정의
    (src/main/resources/countries.xsd)
  - domain 클래스는 XML스키마파일(XSD)에 기반하여 생성
    (Gradle 빌드후 생성 - /build/generated-sources/jaxb)
  - 자바파일 설명
    CountryRepostitory : web service 에 데이터를 제공
    CountryEndpoint : SOAP 요청 처리
    WebServiceConfig : 웹서비스 설정
    Application : 어플리케이션 구동(spring-boot)

6. 테스트
  - SOAP UI 사용
  - 요청  URL : http://localhost:8080/ws
  - WSDL URL : http://localhost:8080/ws/countries.wsdl
  - request
    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
				  xmlns:gs="http://spring.io/guides/gs-producing-web-service">
    <soapenv:Header/>
    <soapenv:Body>
       <gs:getCountryRequest>
          <gs:name>Spain</gs:name>
       </gs:getCountryRequest>
    </soapenv:Body>
    </soapenv:Envelope>

  - response
    <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
    <SOAP-ENV:Header/>
    <SOAP-ENV:Body>
      <ns2:getCountryResponse xmlns:ns2="http://spring.io/guides/gs-producing-web-service">
         <ns2:country>
            <ns2:name>Spain</ns2:name>
            <ns2:population>46704314</ns2:population>
            <ns2:capital>Madrid</ns2:capital>
            <ns2:currency>EUR</ns2:currency>
         </ns2:country>
      </ns2:getCountryResponse>
    </SOAP-ENV:Body>
    </SOAP-ENV:Envelope>

7. 참고 사이트
  - 스프링 웹서비스 가이드
  - http://spring.io/guides/gs/producing-web-service/